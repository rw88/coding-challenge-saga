package filechanreader

import (
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
	"path/filepath"
	"testing"
)

func TestNewFileChanReader(t *testing.T) {
	logger := zaptest.NewLogger(t)

	// Given a predefined file input
	filePath := filepath.Join("..", "..", "testdata", "input-1.txt")

	// When load the file using a channel reader
	ch := NewFileChanReader(filePath, logger.Sugar())

	// Then it should be able to yield the expected values
	assert.Equal(t, "Foo north=Bar west=Baz south=Qu-ux", <-ch)
	assert.Equal(t, "Bar south=Foo west=Bee", <-ch)
}
