package filechanreader

import (
	"bufio"
	"go.uber.org/zap"
	"os"
)

// NewFileChanReader reads a file line by line and yields the results in a channel
func NewFileChanReader(filePath string, logger *zap.SugaredLogger) <-chan string {
	ch := make(chan string)

	go func() {
		file, err := os.Open(filePath)
		if err != nil {
			logger.Fatal(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)

		for scanner.Scan() {
			ch <- scanner.Text()
		}

		if err := scanner.Err(); err != nil {
			logger.Fatal(err)
		}

		close(ch)
	}()

	return ch
}
