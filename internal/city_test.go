package internal

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewCity(t *testing.T) {
	// Given a non-existing city object

	// When a NewCity struct is initialized
	actual := NewCity("dummy")

	// Then the NewCity struct matches the expected state
	assert.Equal(t, "dummy", actual.GetName())
	assert.Equal(t, false, actual.IsDestroyed())
	assert.Equal(t, 0, len(actual.aliensInCity))
	assert.IsType(t, make(chan<- *AlienInteractionMessage), actual.GetInteractionChan())
	assert.IsType(t, &Roads{}, actual.GetRoads())
}
