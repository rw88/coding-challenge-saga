package internal

import (
	"errors"
	"golang.org/x/exp/slices"
	"math/rand"
	"strings"
)

var (
	ErrInvalidRoadName = errors.New("invalid road name")
	ErrDuplicatedRoad  = errors.New("duplicated road")
	ErrCityIsDestroyed = errors.New("city is destroyed")
)

// NewCity creates a new city struct
func NewCity(name string) *City {
	return &City{
		name:            name,
		roads:           newRoads(),
		interactionChan: make(chan *AlienInteractionMessage),
	}
}

// City represents a city in the simulation
type City struct {
	name            string
	roads           *Roads
	interactionChan chan *AlienInteractionMessage
	destroyed       bool
	aliensInCity    []*Alien
}

// GetInteractionChan provides a channel that Aliens use to interact with the city
func (c *City) GetInteractionChan() chan<- *AlienInteractionMessage {
	return c.interactionChan
}

// WaitForInteractions listens on channel for possible alien interactions
func (c *City) WaitForInteractions(done <-chan struct{}) {
	go c.roads.WaitForInteractions(done)

	for {
		select {
		case <-done:
			return
		case m := <-c.interactionChan:
			switch m.action {
			case Leave:
				m.answerChan <- &CityInteractionMessage{
					err: c.alienLeaves(m.alien),
				}
			case Arrive:
				m.answerChan <- &CityInteractionMessage{
					err: c.alienArrives(m.alien),
				}
			}
		}
	}

}

// GetName provides the city name
func (c *City) GetName() string {
	return c.name
}

// GetRoads returns the city exit roads
func (c *City) GetRoads() *Roads {
	return c.roads
}

// IsDestroyed checks is the city is destroyed
func (c *City) IsDestroyed() bool {
	return c.destroyed
}

// Destroy marks the city as destroyed
func (c *City) Destroy() {
	c.destroyed = true

	c.roads.GetRoadsChan() <- &RoadInteractionMessage{
		action: DestroyConnections,
	}
}

// alienArrives simulates an alien arrival
func (c *City) alienArrives(alien *Alien) error {
	alien.SetCurrentCity(c)

	if c.destroyed {
		return ErrCityIsDestroyed
	}

	var enemyAlien *Alien

	if len(c.aliensInCity) > 0 {
		enemyAlien = c.aliensInCity[rand.Intn(len(c.aliensInCity))]
	}

	if enemyAlien != nil {
		alien.Attack(enemyAlien)
		c.removeAlienFromTheCity(enemyAlien)
	} else {
		c.aliensInCity = append(c.aliensInCity, alien)
		alien.SetCurrentCity(c)
	}

	return nil
}

// alienArrives simulates an alien retreat from the city
func (c *City) alienLeaves(alien *Alien) error {
	if !alien.IsAlive() {
		return ErrAlienIsDeadAlready
	}

	if c.IsDestroyed() {
		// Alien cannot leave, the city is destroyed
		return ErrCityIsDestroyed
	}

	c.removeAlienFromTheCity(alien)

	alien.SetCurrentCity(nil)

	return nil
}

// AlienStartsInTheCity puts the alien in a given city during the first step of the simulation
func (c *City) AlienStartsInTheCity(alien *Alien) {
	c.aliensInCity = append(c.aliensInCity, alien)

	alien.SetCurrentCity(c)
}

// removeAlienFromTheCity removes the alien from the list of aliens in the city
func (c *City) removeAlienFromTheCity(alien *Alien) {
	removeIdx := slices.IndexFunc(c.aliensInCity, func(a *Alien) bool {
		return a.GetId() == alien.GetId()
	})

	c.aliensInCity = append(c.aliensInCity[:removeIdx], c.aliensInCity[removeIdx+1:]...)
}

// ToString prints the city in the same format as the input file
func (c *City) ToString() string {
	parts := make([]string, 0, 4)
	parts = append(parts, c.name, c.roads.ToString())

	return strings.Join(parts, " ")
}
