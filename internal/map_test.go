package internal

import (
	"github.com/stretchr/testify/assert"
	"golang.org/x/exp/slices"
	"testing"
)

func TestNewWorldMapTest(t *testing.T) {
	ch := make(chan string)

	// Give key-value pairs representing a connected graph of cities

	go func() {
		ch <- "Foo north=Bar west=Baz south=Qu-ux"
		ch <- "Bar south=Foo west=Bee"

		close(ch)
	}()

	// When the NewWorldMap is invoked with a channel that yields value of the mentioned structure
	wm, err := NewWorldMap(ch)
	assert.NoError(t, err)

	// Then wm.cityNames should be able to match the unique names
	assert.ElementsMatch(t, []string{"Foo", "Bar", "Baz", "Qu-ux", "Bee"}, wm.cityNames)

	// Then wm.cityMap should provide a valid tree structure
	assert.Equal(t, 3, len(wm.cityMap["Foo"].roads.connections))
	assert.Equal(t, "Bar", wm.cityMap["Foo"].roads.connections[RoadNorth].GetName())
	assert.Equal(t, "Baz", wm.cityMap["Foo"].roads.connections[RoadWest].GetName())
	assert.Equal(t, "Qu-ux", wm.cityMap["Foo"].roads.connections[RoadSouth].GetName())

	assert.Equal(t, 2, len(wm.cityMap["Bar"].roads.connections))
	assert.Equal(t, "Foo", wm.cityMap["Bar"].roads.connections[RoadSouth].GetName())
	assert.Equal(t, "Bee", wm.cityMap["Bar"].roads.connections[RoadWest].GetName())

	assert.Equal(t, 1, len(wm.cityMap["Qu-ux"].roads.connections))
	assert.Equal(t, "Foo", wm.cityMap["Qu-ux"].roads.connections[RoadNorth].GetName())

	assert.Equal(t, 1, len(wm.cityMap["Bee"].roads.connections))
	assert.Equal(t, "Bar", wm.cityMap["Bee"].roads.connections[RoadEast].GetName())

	assert.Equal(t, 1, len(wm.cityMap["Baz"].roads.connections))
	assert.Equal(t, "Foo", wm.cityMap["Baz"].roads.connections[RoadEast].GetName())
}

func TestGetAny(t *testing.T) {
	ch := make(chan string)
	// Give key-value pairs representing a connected graph of cities

	go func() {
		ch <- "Foo north=Bar west=Baz south=Qu-ux"
		ch <- "Bar south=Foo west=Bee"

		close(ch)
	}()

	// When the NewWorldMap is invoked with a channel that yields value of the mentioned structure
	wm, err := NewWorldMap(ch)
	assert.NoError(t, err)

	// And when GetAny method is invoked
	actualCity := wm.GetAnyCity()

	// Then the resulting city should be a valid city struct
	assert.IsType(t, &City{}, actualCity)

	// And the city name should match any of the expected values
	expectedValues := []string{"Foo", "Bar", "Baz", "Qu-ux", "Bee"}
	actualIndex := slices.IndexFunc(expectedValues, func(expectedName string) bool {
		return expectedName == actualCity.GetName()
	})
	assert.Greater(t, actualIndex, -1)
}
