package internal

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewAlien(t *testing.T) {
	// Given a new desired alien id (512)

	// When NewAlien function is invoked
	actual := NewAlien(512)

	// Then the default settings for a new alien should be in place, and the id should match
	assert.Equal(t, 512, actual.GetId())
	assert.Equal(t, true, actual.IsAlive())
	assert.Equal(t, false, actual.IsStuck())
}

func TestDie(t *testing.T) {
	// Given an alien alive
	actual := NewAlien(1)
	assert.Equal(t, true, actual.IsAlive())

	// When Die method is invoked
	actual.Die()

	// Then the alien should be dead
	assert.Equal(t, false, actual.IsAlive())
}

func TestStuck(t *testing.T) {
	// Given an alien not stuck
	actual := NewAlien(1)
	assert.Equal(t, false, actual.IsStuck())

	// When SetStuck(true) method is invoked
	actual.SetStuck(true)

	// Then the alien should be stuck
	assert.Equal(t, true, actual.IsStuck())
}

func TestCanMove(t *testing.T) {
	// Given an alien not stuck and alive
	actual := NewAlien(1)

	// When actual.CanMove() is invoked, then the alien should be able to move
	assert.Equal(t, true, actual.CanMove())

	// When the alien is stuck, then it cannot move
	assert.Equal(t, false, actual.IsStuck())

	// Given an alien not stuck and alive
	actual = NewAlien(1)

	// When the alien dies, then it cannot move anymore
	actual.Die()

	assert.Equal(t, false, actual.CanMove())
}

func TestSetCurrentCity(t *testing.T) {
	// Given an alien with no current city set
	actual := NewAlien(1)
	assert.Empty(t, actual.currCity)

	// When SetCurrentCity is invoked with a valid city object
	testCity := NewCity("dummy")
	actual.SetCurrentCity(testCity)

	// Then the current city the alien is in should match the provided city object
	assert.Equal(t, testCity, actual.currCity)
}

func TestAttack(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	// Given a non-destroyed city
	city := NewCity("dummy-city")
	go city.WaitForInteractions(done)
	assert.Equal(t, false, city.IsDestroyed())

	// Given the city is connected to another non-destroyed city
	anotherCity := NewCity("another-dummy-city")
	err := city.roads.SetRoad(RoadEast, anotherCity)
	assert.NoError(t, err)

	go anotherCity.WaitForInteractions(done)
	assert.Equal(t, anotherCity, city.roads.connections[RoadEast])

	// Given two alive aliens that are in the same city
	alien1 := NewAlien(1)
	alien1.SetCurrentCity(city)
	assert.Equal(t, true, alien1.IsAlive())

	alien2 := NewAlien(2)
	alien2.SetCurrentCity(city)
	assert.Equal(t, true, alien2.IsAlive())

	// When the aliens fight
	alien1.Attack(alien2)

	// Then both alies die
	assert.Equal(t, false, alien1.IsAlive())
	assert.Equal(t, false, alien2.IsAlive())

	// And the city gets destroyed and disconnected from the other non-destroyed city
	assert.Empty(t, city.roads.connections)

}
