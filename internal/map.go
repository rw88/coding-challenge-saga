package internal

import (
	"github.com/pkg/errors"
	"math/rand"
	"strings"
)

const (
	keyValueSeparator = "="
)

var (
	ErrDuplicatedCity    = errors.New("the input file has duplicated city entries")
	ErrInvalidTokenEntry = errors.New("invalid token entry")
)

// WorldMap is a key-value map that represents all cities in the simulation
type WorldMap struct {
	cityMap   map[string]*City
	cityNames []string
}

// NewWorldMap creates a new WorldMap struct
func NewWorldMap(ch <-chan string) (*WorldMap, error) {
	worldMap := &WorldMap{
		cityMap: make(map[string]*City),
	}

	uniqueCityEntries := make(map[string]*City)

	for line := range ch {
		parts := strings.Split(line, " ")

		currCityName := parts[0]

		// Check duplicated entries
		if _, ok := uniqueCityEntries[currCityName]; ok {
			return nil, ErrDuplicatedCity
		}

		city, cityFound := worldMap.cityMap[currCityName]

		if !cityFound {
			city = NewCity(currCityName)
			worldMap.AddCity(currCityName, city)
		}

		uniqueCityEntries[currCityName] = city

		// Assumption: not all roads need to be defined, but at least one needs to be.
		for i := 1; i < len(parts); i++ {
			if !strings.Contains(parts[i], keyValueSeparator) {
				return nil, errors.Wrapf(ErrInvalidTokenEntry, "missing separator")
			}

			roadParts := strings.Split(parts[i], keyValueSeparator)

			if len(roadParts) != 2 {
				return nil, errors.Wrapf(ErrInvalidTokenEntry, "expected 2 values after the road key/value split")
			}

			roadName := strings.ToLower(roadParts[0])
			targetCityName := roadParts[1]

			targetCity, cityFound := worldMap.cityMap[targetCityName]

			if !cityFound {
				targetCity = NewCity(targetCityName)

				worldMap.AddCity(targetCityName, targetCity)
			}

			if err := city.roads.SetRoad(roadName, targetCity); err != nil {
				return nil, errors.Wrap(err, "city.roads.SetRoad failed")
			}

			oppositeRoad := roadDirections[roadName]
			if err := targetCity.roads.SetRoad(oppositeRoad, city); err != nil {
				return nil, errors.Wrap(err, "targetCity.roads.SetRoad failed")
			}

		}
	}

	return worldMap, nil
}

// AddCity adds a city in the world map
func (wm *WorldMap) AddCity(cityName string, city *City) {
	if _, cityFound := wm.cityMap[cityName]; cityFound {
		return
	}

	wm.cityMap[cityName] = city
	wm.cityNames = append(wm.cityNames, cityName)
}

// GetAnyCity retrieves a random city
func (wm *WorldMap) GetAnyCity() *City {
	return wm.cityMap[wm.cityNames[rand.Intn(len(wm.cityNames))]]
}
