package internal

type Action int

const (
	Arrive Action = iota
	Leave
)

// AlienInteractionMessage represents an interaction between the alien and the city
type AlienInteractionMessage struct {
	alien      *Alien
	action     Action
	answerChan chan *CityInteractionMessage
}

// CityInteractionMessage represents the answer from the city to the alien interaction
type CityInteractionMessage struct {
	err error
}
