// Package internal provides all the logic required to run the alien invasion simulation
package internal

import (
	"fmt"
	"sync"
)

func InitializeAliensInTheWorldMap(numAliens int, worldMap *WorldMap) []*Alien {
	aliens := make([]*Alien, 0, numAliens)
	for i := 1; i <= numAliens; i++ {
		alien := NewAlien(i)
		aliens = append(aliens, alien)

		worldMap.GetAnyCity().AlienStartsInTheCity(alien)
	}

	return aliens
}

// RunWorld starts the simulation
func RunWorld(worldMap *WorldMap, aliens []*Alien, numIterations int) {
	done := make(chan struct{})
	defer close(done)

	for _, cityName := range worldMap.cityNames {
		go worldMap.cityMap[cityName].WaitForInteractions(done)
	}

	iterationsCount := 0
	for i := 0; i < numIterations; i++ {
		wg := &sync.WaitGroup{}

		aliensAlive := false
		for _, alien := range aliens {
			if alien.CanMove() {
				aliensAlive = true

				wg.Add(1)

				go alien.Move(wg)
			}
		}

		wg.Wait()

		iterationsCount++

		if !aliensAlive {
			break
		}
	}

	//printDebugStats(aliens, iterationsCount)
	printRemainingObjects(worldMap, aliens, iterationsCount)
}

// printRemainingObjects prints the remaining objects after the simulation
func printRemainingObjects(worldMap *WorldMap, aliens []*Alien, iterationsCount int) {

	fmt.Println()
	fmt.Printf("Remaining cities:\n")

	totalRemaining := 0

	for _, cityName := range worldMap.cityNames {
		city := worldMap.cityMap[cityName]

		if city.IsDestroyed() {
			continue
		}

		fmt.Println(worldMap.cityMap[cityName].ToString())

		totalRemaining++
	}

	if totalRemaining == 0 {
		fmt.Printf("No remaining cities\n")
	}

	fmt.Println()
	fmt.Printf("Total iterations: %d\n", iterationsCount)
}
