package internal

import (
	"fmt"
	"github.com/pkg/errors"
	"golang.org/x/exp/slices"
	"math/rand"
	"strings"
)

const (
	RoadNorth = "north"
	RoadSouth = "south"
	RoadWest  = "west"
	RoadEast  = "east"
)

// roadDirections is a key-value map that is used for validation and demonstrate road directions
var roadDirections = map[string]string{
	RoadNorth: RoadSouth,
	RoadSouth: RoadNorth,
	RoadWest:  RoadEast,
	RoadEast:  RoadWest,
}

var (
	ErrCityHasNoExitRoads = errors.New("city has no exist rows")
)

type RoadChanAction int

const (
	GetRandomRoad RoadChanAction = iota
	DestroyConnections
)

// RoadInteractionMessage represents an interaction between an alien/city and a road
type RoadInteractionMessage struct {
	action     RoadChanAction
	answerChan chan *RoadAnswerMessage
}

// RoadAnswerMessage represents the answer from the road to the alien interaction
type RoadAnswerMessage struct {
	err        error
	targetCity *City
}

// Roads represents the roads that a given city can access
type Roads struct {
	connections map[string]*City
	names       []string
	roadChan    chan *RoadInteractionMessage
}

// newRoads creates a new Roads struct
func newRoads() *Roads {
	return &Roads{
		connections: map[string]*City{},
		roadChan:    make(chan *RoadInteractionMessage),
	}
}

// GetRoadsChan roads channel
func (r *Roads) GetRoadsChan() chan<- *RoadInteractionMessage {
	return r.roadChan
}

// SetRoad sets a road connection
func (r *Roads) SetRoad(roadName string, toCity *City) error {
	if _, ok := roadDirections[roadName]; !ok {
		return ErrInvalidRoadName
	}

	if connectedCity, ok := r.connections[roadName]; ok {
		if connectedCity.GetName() != toCity.GetName() {
			return ErrDuplicatedRoad
		}
		return nil
	}

	r.connections[roadName] = toCity
	r.names = append(r.names, roadName)

	return nil
}

// WaitForInteractions provides listen to road related communication channels
func (r *Roads) WaitForInteractions(done <-chan struct{}) {
	for {
		select {
		case <-done:
			return
		case m := <-r.roadChan:
			switch m.action {
			case GetRandomRoad:
				targetCity, err := r.getRandomRoad()
				m.answerChan <- &RoadAnswerMessage{
					targetCity: targetCity,
					err:        err,
				}
			case DestroyConnections:
				r.destroyConnections()
			}
		}
	}
}

// destroyConnections removes all connections from a given city
func (r *Roads) destroyConnections() {
	for _, roadName := range r.names {

		delete(r.connections, roadName)
	}

	r.names = nil
}

// destroyRoad removes connections to a given city
func (r *Roads) destroyRoad(roadNameA string) {
	//delete(r.connections, roadNameA)

	idx := slices.IndexFunc(r.names, func(roadNameB string) bool {
		return roadNameA == roadNameB
	})

	r.names = append(r.names[:idx], r.names[idx+1:]...)
}

// getRandomRoad provides a random road connection
func (r *Roads) getRandomRoad() (*City, error) {
	if len(r.names) == 0 {
		return nil, ErrCityHasNoExitRoads
	}

	nextRoadName := r.names[rand.Intn(len(r.names))]

	if r.connections[nextRoadName].IsDestroyed() {
		r.destroyRoad(nextRoadName)

		return r.getRandomRoad()
	}

	return r.connections[nextRoadName], nil
}

// ToString prints the city in the same format as the input file
func (r *Roads) ToString() string {
	parts := make([]string, 0, 4)

	for _, roadName := range r.names {
		city := r.connections[roadName]

		if city.IsDestroyed() {
			continue
		}

		parts = append(parts, fmt.Sprintf("%s=%s", roadName, city.GetName()))
	}

	return strings.Join(parts, " ")
}
