package internal

import (
	"fmt"
	"github.com/pkg/errors"
	"sync"
)

var (
	ErrAlienIsDeadAlready = errors.New("alien is already dead")
)

// NewAlien creates a new Alien struct
func NewAlien(id int) *Alien {
	return &Alien{
		id:    id,
		alive: true,
		stuck: false,
	}
}

// Alien struct represent an alien in the simulation
type Alien struct {
	id       int
	alive    bool
	stuck    bool
	currCity *City
	lock     sync.RWMutex
}

// IsAlive checks is the alien is alive or not
func (a *Alien) IsAlive() bool {
	a.lock.RLock()
	defer a.lock.RUnlock()

	return a.alive
}

// IsStuck checks if the alien is stuck
func (a *Alien) IsStuck() bool {
	a.lock.RLock()
	defer a.lock.RUnlock()

	return a.stuck
}

// CanMove checks if the alien can move
func (a *Alien) CanMove() bool {
	a.lock.RLock()
	defer a.lock.RUnlock()

	return a.alive && !a.stuck
}

// SetStuck marks the alien as stuck or not
func (a *Alien) SetStuck(stuck bool) {
	a.lock.Lock()
	defer a.lock.Unlock()

	a.stuck = stuck
}

// GetId provides the alien id
func (a *Alien) GetId() int {
	return a.id
}

// Die marks the alien as dead
func (a *Alien) Die() {
	a.lock.Lock()
	defer a.lock.Unlock()

	a.alive = false
}

// SetCurrentCity defines the city the alien is in
func (a *Alien) SetCurrentCity(city *City) {
	a.currCity = city
}

// Attack attacks an enemy alien
func (a *Alien) Attack(enemyAlien *Alien) {
	a.currCity.Destroy()
	enemyAlien.Die()
	a.Die()

	fmt.Printf("%s has been destroyed by alien %d and alien %d!\n",
		a.currCity.GetName(),
		a.GetId(),
		enemyAlien.GetId(),
	)
}

// Move makes the alien move to another city
func (a *Alien) Move(wg *sync.WaitGroup) bool {
	defer wg.Done()

	roadCh := make(chan *RoadAnswerMessage)
	a.currCity.GetRoads().GetRoadsChan() <- &RoadInteractionMessage{
		action:     GetRandomRoad,
		answerChan: roadCh,
	}
	roadAnswer := <-roadCh
	nextCity := roadAnswer.targetCity

	if errors.Is(roadAnswer.err, ErrCityHasNoExitRoads) {
		a.SetStuck(true)
		// Do nothing, Alien is stuck
		// Assumption: It is assumed that the roads are not necessarily two-way roads
		// So it's possible that the Alien gets "stuck" in a city where there is no exit (in case some city doesn't have exits defined in the input file)
		return false
	}

	ch := make(chan *CityInteractionMessage)

	a.currCity.GetInteractionChan() <- &AlienInteractionMessage{
		alien:      a,
		action:     Leave,
		answerChan: ch,
	}

	answer := <-ch

	if errors.Is(answer.err, ErrCityIsDestroyed) {
		return false
	}

	ch = make(chan *CityInteractionMessage)
	nextCity.GetInteractionChan() <- &AlienInteractionMessage{
		alien:      a,
		action:     Arrive,
		answerChan: ch,
	}

	<-ch

	return true
}
