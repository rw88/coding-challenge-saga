package cmd

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/rw88/coding-challenge-saga/internal"
	"gitlab.com/rw88/coding-challenge-saga/pkg/filechanreader"
	"go.uber.org/zap"
)

var (
	ErrInvalidArguments = errors.New("invalid number of arguments")
	ErrInvalidFlags     = errors.New("required flags are missing")
)

const (
	flagNumAliens = "num_aliens"
)

// NewRunCommand provides a Cobra struct
func NewRunCommand(logger *zap.SugaredLogger) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "run file_input",
		Short: "Run Alien Invasion simulation",
		Long: fmt.Sprintf(`Usage:
		%s run fileinput.txt -n 1000
		`, internal.AppName),
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return errors.Wrap(ErrInvalidArguments, "run command expect a single argument")
			}

			n, _ := cmd.Flags().GetInt(flagNumAliens)

			if n <= 0 {
				return errors.Wrap(ErrInvalidFlags, "flag 'n' is missing")
			}

			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			filePath := args[0]
			numAliens, _ := cmd.Flags().GetInt(flagNumAliens)

			ch := filechanreader.NewFileChanReader(filePath, logger)

			worldMap, err := internal.NewWorldMap(ch)
			if err != nil {
				logger.Fatal(errors.Wrap(err, "internal.NewWorldMap failed"))
			}

			aliens := internal.InitializeAliensInTheWorldMap(numAliens, worldMap)

			internal.RunWorld(worldMap, aliens, internal.MaxNumIterations)
		},
	}

	cmd.Flags().IntP(flagNumAliens, "n", 0, "number of aliens")

	return cmd
}
