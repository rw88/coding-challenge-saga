// Package cmd provides CLI routing and arguments/flags parsing
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/rw88/coding-challenge-saga/internal"
	"go.uber.org/zap"
)

// NewRootCommand provides a Cobra struct
func NewRootCommand(logger *zap.SugaredLogger) *cobra.Command {
	rootCmd := &cobra.Command{
		Use:   internal.AppName,
		Short: "An Alien Invasion simulator",
		Long:  `An Alien Invasion simulator`,
	}
	rootCmd.CompletionOptions.DisableDefaultCmd = true

	rootCmd.AddCommand(NewRunCommand(logger))

	return rootCmd
}
