// Package main is the simulation starting point
package main

import (
	"gitlab.com/rw88/coding-challenge-saga/cmd/alieninvasion/cmd"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	logger := initLogger()

	cmd := cmd.NewRootCommand(logger)

	err := cmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// initLogger provides a logger struct
func initLogger() *zap.SugaredLogger {
	config := zap.NewProductionEncoderConfig()
	config.EncodeTime = zapcore.ISO8601TimeEncoder
	fileEncoder := zapcore.NewJSONEncoder(config)

	logFile, err := os.OpenFile(filepath.Join("var", "logs", "log.json"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	writer := zapcore.AddSync(logFile)
	defaultLogLevel := zapcore.DebugLevel
	core := zapcore.NewTee(
		zapcore.NewCore(fileEncoder, writer, defaultLogLevel),
	)

	logger := zap.New(core, zap.AddCaller(), zap.AddStacktrace(zapcore.ErrorLevel))

	defer logger.Sync() // nolint
	sugar := logger.Sugar()

	return sugar
}
