# Saga's Coding Challenge: "Alien Invasion"

Alien Invasion is an application that simulates an alien invasion in a given map.

## Dependencies

- GO v1.18 or greater

## Installation
After installing the above-mentioned dependencies, run the command below in the project root folder.

```bash
make build
```
After executing the command, a new file will be created in the project's root folder: **alieninvasion**, which will be used to run the application's commands.

## Usage

### Run command
```bash
alieninvasion inputfile [flags]

Example:
alieninvasion run /var/etc/input.txt -n 20
```


### Help command
To see the documentation provided within the application in general or for a specific command, run the commands below.

```bash
alieninvasion // general docs
alieninvasion help // general docs
alieninvasion run help // command specific docs
```

## Tests
```bash
make build
```

## Lint
Run the linters to ensure that the code follows a predetermined standard coding style.

On Mac, install golangci-lint

```bash
brew install golangci-lint
```

Then run the linter
```bash
make lint
```

## License
[MIT](https://choosealicense.com/licenses/mit/)