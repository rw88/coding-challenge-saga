test:
	go test ./... -v

lint:
	golangci-lint run ./...

build:
	go build cmd/alieninvasion/alieninvasion.go